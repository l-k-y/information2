# -*- coding: utf-8 -*-
# @Time    : 2020/7/14 9:56 下午
# @Author  : dxd


""""
假设前端同学通过接口向后端传了天猫的行业信息，例如：
industry_list = [
  {
     "parent_ind" : "女装",
     "name" : "连衣裙"
  },
  {
     "name": "女装"
  },
  {
     "parent_ind" : "女装",
     "name" : "半身裙"
  },
  {
     "parent_ind" : "女装",
     "name" : "A字裙"
  },
  {
     "name": "数码"
  },
  {
    "parent_ind" : "数码",
     "name": "电脑配件"
  },
  {
    "parent_ind" : "电脑配件",
     "name": "内存"
  },
]

为了取用方便，我们希望可以将其转换为树状格式，例如：
{
  "数码": {
    "电脑配件": {
        "内存" : {}
     }
  },
  "女装" : {
     "连衣裙": {},
    "半身裙": {},
    "A字裙": {}
  }
}
实现一个方法完成这个转换
function convert_format(data)

"""

industry_list = [
  {
     "parent_ind" : "女装",
     "name" : "连衣裙"
  },
  {
     "name": "女装"
  },
  {
     "parent_ind" : "女装",
     "name" : "半身裙"
  },
  {
     "parent_ind" : "女装",
     "name" : "A字裙"
  },
  {
     "name": "数码"
  },
  {
    "parent_ind" : "数码",
     "name": "电脑配件"
  },
  {
    "parent_ind" : "电脑配件",
     "name": "内存"
  },
    {
    "parent_ind" : "内存",
     "name": "内存条"
  },{
    "parent_ind" : "内存条",
     "name": "金士顿内存条"
  },{
    "parent_ind" : "内存",
     "name": "内存条1"
  },
]

""""
通过递归，找出树形的所有子集，可以求出多级结构，一开始不使用，可以通过三个for求出答案，
但是如果超过三级就没法计算，局限性大，后通过添加全局变量，使用递归和列表推导式可以求出多级
{
    '女装':{
        '连衣裙':{}, 
        '半身裙':{},
        'A字裙': {}
        },
    '数码':{
        '电脑配件':{
            '内存':{
                '内存条':{
                    '金士顿内存条':{}
                },
                '内存条1':{}
            }
        }
    }
}
"""
root_dict = {}


def select_child(roots, childs):
    if len(childs) == 0:
        return root_dict
    child_list = []
    for child in childs:
        for root in roots.values():
            if child["parent_ind"] in root.keys():
                root[child["parent_ind"]][child["name"]] = {}
            else:
                child_list.append(child)
    key_dict = {}
    [key_dict.update(i) for i in roots.values()]
    return select_child(key_dict, child_list)


def convert_format(data):
    child_list = []
    for node in data:
        if node.get("parent_ind") is None:
            global root_dict
            root_dict[node["name"]] = {}
            print("+    " + str(root_dict))
        else:
            child_list.append(node)
            print("-    " + str(child_list))
    child_list2 = []
    for child in child_list:
        if child["parent_ind"] in root_dict.keys():
            root_dict[child["parent_ind"]][child["name"]] = {}
            print("1   "+str(root_dict))
        else:
            child_list2.append(child)
            print("2   "+str(child_list2))
    return select_child(root_dict, child_list2)


print(convert_format(industry_list))