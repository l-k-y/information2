# class Test():
#     def __call__(self):
#         print('call me!')
#
# t = Test
# t()()

""""
假设前端同学通过接口向后端传了天猫的行业信息，例如：
industry_list = [
  {
     "parent_ind" : "女装",
     "name" : "连衣裙"
  },
  {
     "name": "女装"
  },
  {
     "parent_ind" : "女装",
     "name" : "半身裙"
  },
  {
     "parent_ind" : "女装",
     "name" : "A字裙"
  },
  {
     "name": "数码"
  },
  {
    "parent_ind" : "数码",
     "name": "电脑配件"
  },
  {
    "parent_ind" : "电脑配件",
     "name": "内存"
  },
]

为了取用方便，我们希望可以将其转换为树状格式，例如：
{
  "数码": {
    "电脑配件": {
        "内存" : {}
     }
  },
  "女装" : {
     "连衣裙": {},
    "半身裙": {},
    "A字裙": {}
  }
}
实现一个方法完成这个转换
function convert_format(data)

"""
industry_list = [
  {
     "parent_ind" : "女装",
     "name" : "连衣裙"
  },
  {
     "name": "女装"
  },
  {
     "parent_ind" : "女装",
     "name" : "半身裙"
  },
  {
     "parent_ind" : "女装",
     "name" : "A字裙"
  },
  {
     "name": "数码"
  },
  {
    "parent_ind" : "数码",
     "name": "电脑配件"
  },
  {
    "parent_ind" : "电脑配件",
     "name": "内存"
  },
]


"""
{
  "数码": {
    "电脑配件": {
        "内存" : {}
     }
  },
  "女装" : {
     "连衣裙": {},
    "半身裙": {},
    "A字裙": {}
  }
}"""

def func(data):
    key1 = ""
    list = []
    dict_one = {}
    dict_two = {}

    for i in data:
        if "parent_ind" in i :
            # dict_one[i.get("parent_ind")] = dict_two[i.get("name")]
            dict_two[i.get("name")] = {}
            dict_two[i.get("name")][i.get("parent_ind")] = {}
            dict_two[i.get("name")][i.get("parent_ind")] = i.get("name")
        else:
            dict_one[i.get("name")] = {}

    print(dict_one)
    print()
    print(dict_two)

    #
    #
    #     if i.get("name") not in list:
    #         list.append(i.get("name"))
    #     if i.get("parent_ind") in list:
    #         dict_one[]
    # return


if __name__ == '__main__':
    func(industry_list)



