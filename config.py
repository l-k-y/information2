import logging

from redis import StrictRedis


class Config(object):
    """项目的配置"""

    SECRET_KEY = "WRsLPbEhA3IpJb+b/EeJSDddoUnIecmFREzOvjFq5xKBScgSisD8OaO3JHyVMIvO"

    # 为MySql添加配置
    SQLALCHEMY_DATABASE_URI = "mysql://root:123456@127.0.0.1:3306/information27"
    SQLALCHEMY_TRACK_MODIFICATIONS = False
    SQLALCHEMY_COMMIT_ON_TEARDOWN = True
    # 为Redis添加配置
    REDIS_HOST = '127.0.0.1'
    REDIS_PORT = 6379

    # 设置session类型
    SESSION_TYPE = "redis"
    # 设置开启session签名（加签）
    SESSION_USE_SIGNER = True
    # 指定 session 保存 redis
    SESSION_REDIS = StrictRedis(host=REDIS_HOST, port=REDIS_PORT)
    # 设置session过期
    SESSION_PERMANENT = False
    # 设置过期时间
    PERMANENT_SESSION_LIFETIME = 86400 * 2

    # 设置日志等级
    LOG_LEVEL = logging.DEBUG


class DevelopmentConfig(Config):
    """开发环境"""
    DEBUG = True


class ProductionConfig(Config):
    """生产环境"""
    DEBUG = False
    LOG_LEVEL = logging.WARNING


class TestingConfig(Config):
    """单元测试环境"""
    DEBUG = True
    TESTING = True


config = {
    "development": DevelopmentConfig,
    "production": ProductionConfig,
    "testing" : TestingConfig
}
