import functools
from flask import session, current_app, g


# 公共的自定义工具类
from info.models import User


def do_index_class(index):
    """返回指定索引对应的类名"""
    # 过滤器
    if index == 0:
        return "first"
    elif index == 1:
        return "second"
    elif index == 2:
        return "third"
    return ""


def user_login_data(f):
    # 保持内层函数的__name__不变，不使用则会使内层函数名都变为wrapper
    @functools.wraps(f)
    def wrapper(*args, **kwargs):
        # 查询用户是否登陆
        user_id = session.get("user_id")
        user = None
        if user_id:
            try:
                user = User.query.get(user_id)
            except Exception as e:
                current_app.logger.error(e)
        g.user = user
        return f(*args, **kwargs)
    return wrapper