var currentCid = 1; // 当前分类 id
var cur_page = 1; // 当前页
var total_page = 1;  // 总页数
var data_querying = true;   // 是否正在向后台获取数据


$(function () {
    // 加载页面取调用这个方法请求news_list
    updateNewsData()

    // 首页分类切换
    $('.menu li').click(function () {
        // 获取当前得cid
        var clickCid = $(this).attr('data-cid')
        // li其他标签each
        $('.menu li').each(function () {
            // 移除选中状态
            $(this).removeClass('active')
        })
        $(this).addClass('active')

        if (clickCid != currentCid) {
            // 记录当前分类id
            currentCid = clickCid

            // 重置分页参数
            cur_page = 1
            total_page = 1
            updateNewsData()
        }
    })

    //页面滚动加载相关
    $(window).scroll(function () {

        // 浏览器窗口高度
        var showHeight = $(window).height();

        // 整个网页的高度
        var pageHeight = $(document).height();

        // 页面可以滚动的距离
        var canScrollHeight = pageHeight - showHeight;

        // 页面滚动了多少,这个是随着页面滚动实时变化的
        var nowScroll = $(document).scrollTop();

        if ((canScrollHeight - nowScroll) < 100) {
            // 判断页数，去更新新闻数据
            if (!data_querying){
                // 将 【正在请求数据变量】置为true 让其请求数据
                data_querying = true
                // 如果当前页小于总页数采取请求数据
                if (cur_page < total_page){
                    // 将当前页+1
                    cur_page += 1
                    // 去加载数据
                    updateNewsData()
                }
            }

        }
    })
})

function updateNewsData() {
    // 更新新闻数据
    var params = {
        "cid":currentCid,
        "page":cur_page,
    }
    $.get("/news_list", params, function (resp) {
        // 发起请求后吧【是否正在向后台获取数据】变量置为false 不让其再去请求调起查询方法
        data_querying = false
        // 请求成功
        if (resp.errno == "0"){
            // 获取总页数
            total_page = resp.data.total_page
            //清空当前页 只有当前页为第一页时清除数据
            if (cur_page == 1) {
                $(".list_con").html("")
            }
            // 加载后台返回得数据  接口字段要对应
            for (var i=0;i<resp.data.news_dict_li.length;i++) {
                var news = resp.data.news_dict_li[i]
                var content = '<li>'
                content += '<a href="/news/'+ news.id +'" class="news_pic fl"><img src="' + news.index_image_url + '?imageView2/1/w/170/h/170"></a>'
                content += '<a href="/news/'+ news.id +'" class="news_title fl">' + news.title + '</a>'
                content += '<a href="/news/'+ news.id +'" class="news_detail fl">' + news.digest + '</a>'
                content += '<div class="author_info fl">'
                content += '<div class="source fl">来源：' + news.source + '</div>'
                content += '<div class="time fl">' + news.create_time + '</div>'
                content += '</div>'
                content += '</li>'
                $(".list_con").append(content)
            }
        }else {
            // 请求失败
            alert(resp.errmsg)
        }
    })

}
