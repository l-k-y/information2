import random
import re
from datetime import datetime

from flask import request, abort, make_response, current_app, jsonify, session

from . import passport_blu
from ... import redis_store, constants, db
from ...libs.yuntongxun.sms import CCP
from ...models import User
from ...utils.captcha.captcha import captcha
from ...utils.response_code import RET


@passport_blu.route("/logout")
def logout():
    """
    退出登陆
    :return:
    """
    session.pop("user_id", None)
    session.pop("mobile", None)
    session.pop("nick_name", None)
    return jsonify(errno=RET.OK, errmsg="退出成功")


@passport_blu.route("/login", methods=["POST"])
def login():
    """

    :return:
    """
    params = request.json

    mobile = params.get("mobile")
    password = params.get("password")

    if not all([mobile, password]):
        return jsonify(errno=RET.PARAMERR, errmsg="参数错误")
    # 校验手机号是否正确
    if not re.match("1[35478]\\d{9}", mobile):
        return jsonify(errno=RET.PARAMERR, errmsg="手機格式不正確")
    # 数据库查询用户
    try:
        user = User.query.filter(User.mobile == mobile).first()
    except Exception as e:
        current_app.logger.error(e)
        return jsonify(errno=RET.DBERR, errmsg="数据库查询错误")
    # 校验用户是否存在
    if not user:
        return jsonify(errno=RET.NODATA, errmsg="用户不存在")
    # 校验密码是否正确
    if not user.check_passowrd(password):
        return jsonify(errno=RET.PWDERR, errmsg="用户名或密码错误")

    # 往session中添加信息表示已经登陆
    session["user_id"] = user.id
    session["mobile"] = user.mobile
    session["nick_name"] = user.nick_name
    # 记录最后一次登陆时间
    user.last_login = datetime.now()
    # 修改表中的属性内容需要提交，也可修改数据库中的配置默认提交（在config中修改）
    # try:
    #     db.session.commit()
    # except Exception as e:
    #     db.session.rollback()
    #     current_app.logger.error(e)

    return jsonify(errno=RET.OK, errmsg="登陆成功")


@passport_blu.route("/register", methods=["POST"])
def register():
    """
    1.接收参数
    2.校验参数
    3.判断验证码是否正确
    4.数据库中记录用户属性
    5.返回数据
    :return:
    """
    # 接受参数
    params_dict = request.json
    mobile = params_dict.get("mobile")
    password = params_dict.get("password")
    smscode = params_dict.get("smscode")
    # 校验参数
    if not all([mobile, password, smscode]):
        return jsonify(errno=RET.PARAMERR, errmsg="参数错误")
    # 校验手机号是否正确
    if not re.match("1[35478]\\d{9}", mobile):
        return jsonify(errno=RET.PARAMERR, errmsg="手機格式不正確")
    # 从reids中获取正确的短信验证码
    try:
        real_sms_code = redis_store.get("sms_" + mobile)
    except Exception as e:
        current_app.logger.error(e)
        return jsonify(errno=RET.DBERR, errmsg="数据库查询错误")
    # 查看正确验证码是否存在
    if not real_sms_code:
        return jsonify(errno=RET.NODATA, errmsg="短息验证码已过期")
    # 比较验证码是否正确
    if smscode != real_sms_code:
        return jsonify(errno=RET.DATAERR, errmsg="验证码错误")
    # 创建用户
    user = User()
    # 用户手机号
    user.mobile = mobile
    # 用户昵称
    user.nick_name = mobile
    #  用户数据的密码
    user.password = password
    # 记录最后一次登陆时间
    user.last_login = datetime.now()

    # 进行数据库入库
    try:
        db.session.add(user)
        db.session.commit()
    except Exception as e:
        current_app.logger.error(e)
        db.session.rollback()
        return jsonify(errno=RET.DBERR, errmsg="数据库保存失败")

    # 往session中添加信息表示已经登陆
    session["user_id"] = user.id
    session["mobile"] = user.mobile
    session["nick_name"] = user.nick_name

    # 返回响应
    return jsonify(errno=RET.OK, errmsg="注册成功")


@passport_blu.route("/sms_code", methods=["POST"])
def sms_code():
    """
    1.接受前端返回的參數 1.手機號 2.圖片驗證碼 3.驗證碼編號
    2.判斷接收的參數是是否爲空，手機號的格式是夠正確
    3.判斷圖片驗證碼是否正確
    4.向手機號發送短信
    5.存儲短信驗證碼的内容
    6.返回前端數據
    :return:
    """

    # 接受參數
    params_dict = request.json

    mobile = params_dict.get("mobile")
    image_code = params_dict.get("image_code")
    image_code_id = params_dict.get("image_code_id")

    # 判斷參數是否爲空
    if not all([mobile, image_code, image_code_id]):
        # {"errno":4100, "errmsg":"參數有誤"}
        return jsonify(errno=RET.PARAMERR, errmsg="參數有誤")

    # 校驗手機號格式
    if not re.match("1[35478]\\d{9}", mobile):
        return jsonify(errno=RET.PARAMERR, errmsg="手機格式不正確")

    # 從數據庫中獲取圖片驗證碼内容
    try:
        real_image_code = redis_store.get("imageCodeId_" + image_code_id)
    except Exception as e:
        current_app.logger.error(e)
        return jsonify(errno=RET.DBERR, errmsg="数据库查询错误")
    # 檢查圖片驗證碼是否過期
    if not real_image_code:
        return jsonify(errno=RET.NODATA, errmsg="验证码已过期")

    # 判斷圖片驗證碼是否正確 同时变成大写
    if image_code.upper() != real_image_code.upper():
        return jsonify(errno=RET.DATAERR, errmsg="验证码错误")

    # 生成短信验证码
    sms_code_id = "%06d" % random.randint(0, 999999)
    current_app.logger.debug("短信验证码内容为：" + sms_code_id)  # 打印验证码内容

    # 验证码校验成功，发送短信
    # result = CCP().send_template_sms(mobile, [sms_code_id, constants.SMS_CODE_REDIS_EXPIRES/60], 1)
    # if result != 0:
    #     return jsonify(errno=RET.THIRDERR, errmsg="短信发送失败")

    # 存储短信验证码内容到redis
    try:
        redis_store.set("sms_" + mobile, sms_code_id)
    except Exception as e:
        current_app.logger.error(e)
        return jsonify(errno=RET.DBERR, errmsg="数据库保存失败")

    # 返回结果
    return jsonify(errno=RET.OK, errmsg="发送短信成功")


@passport_blu.route("/image_code")
def get_image_code():
    """
    1.接受參數
    2.判斷參數是否爲空
    3.生成圖片驗證碼
    4.保存圖片驗證碼的内容
    5.返回圖片
    :return:
    """

    # 接受參數
    image_code = request.args.get("imageCodeId", None)
    # 判斷參數是否爲None
    if not image_code:
        return abort(403)
    # 生成圖片驗證碼
    name, text, image = captcha.generate_captcha()
    current_app.logger.debug(text)
    # 保存圖片内容
    try:
        redis_store.set("imageCodeId_" + image_code, text, constants.IMAGE_CODE_REDIS_EXPIRES)
    except Exception as e:
        current_app.logger.error(e)
        abort(500)
    # 返回圖片
    response = make_response(image)
    response.headers["Content-Type"] = "image/jpg"
    return response

