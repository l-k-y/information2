from flask import render_template, g, current_app, request, jsonify, abort
from sqlalchemy.testing import in_

from info import db
from info.models import News, Comment, CommentLike, User
from info.modules.news import news_blue
from info.utils.common import user_login_data
from info.utils.response_code import RET


@news_blue.route("/followed_user", methods=["POST", "GET"])
@user_login_data
def followed_user():
    """

    :return:
    """
    user = g.user
    user_id = request.json.get("user_id")
    action = request.json.get("action")

    if not all([user_id, action]):
        return jsonify(errno=RET.PARAMERR, errmsg="参数错误")

    if action not in ("follow", "unfollow"):
        return jsonify(errno=RET.PARAMERR, errmsg="参数错误")

    try:
        target_user = User.query.get(user_id)
    except Exception as e:
        current_app.logger.error(e)
        return jsonify(errno=RET.DBERR, errmsg="数据库查询错误")

    if not target_user:
        return jsonify(errno=RET.NODATA, errmsg="当前用户不存在")

    if action == "follow":
        if target_user.followers.filter(User.id == user.id).count() > 0:
            return jsonify(errno=RET.DATAEXIST, errmsg="当前用户已经关注")
        target_user.followers.append(user)
    else:
        if target_user.followers.filter(User.id == user.id).count() > 0:
            target_user.followers.remove(user)

    return jsonify(errno=RET.OK, errmsg="操作成功")


@news_blue.route("/comment_like", methods=["POST"])
@user_login_data
def comment_like():
    """
    评论点赞
    :return:
    """
    user = g.user

    if not user:
        return jsonify(errno=RET.SESSIONERR, errmsg="用户不存在")

    comment_id = request.json.get("comment_id")
    action = request.json.get("action")  # 动作

    if not all([comment_id, action]):
        return jsonify(errno=RET.PARAMERR, errmsg="参数错误")

    if action not in ["add", "remove"]:
        return jsonify(errno=RET.PARAMERR, errmsg="参数错误")

    try:
        comment_id = int(comment_id)
    except Exception as e:
        current_app.logger.error(e)
        return jsonify(errno=RET.PARAMERR, errmsg="参数错误")

    # 查询时哪条评论点赞
    try:
        comment = Comment.query.get(comment_id)
    except Exception as e:
        current_app.logger.error(e)
        return jsonify(errno=RET.DBERR, errmsg="数据库查询错误")

    if not comment:
        return jsonify(errno=RET.NODATA, errmsg="评论不存在")

    # 点赞
    if action == "add":
        comment_like_model = CommentLike.query.filter(CommentLike.user_id == user.id,
                                                      CommentLike.comment_id == comment_id).first()
        if not comment_like_model:
            comment_like_model = CommentLike()
            comment_like_model.user_id = user.id
            comment_like_model.comment_id = comment_id
            db.session.add(comment_like_model)
            # 更新点赞次数
            comment.like_count += 1
    else:
        comment_like_model = CommentLike.query.filter(CommentLike.user_id == user.id,
                                                      CommentLike.comment_id == comment_id).first()
        if comment_like_model:
            db.session.delete(comment_like_model)
            # 更新点赞次数
            comment.like_count -= 1

    return jsonify(errno=RET.OK, errmsg="OK")


@news_blue.route("/news_comment", methods=["POST"])
@user_login_data
def comment_news():
    """
    接收参数s
    校验参数
    入库返回
    :return:
    """

    user = g.user

    if not user:
        return jsonify(errno=RET.SESSIONERR, errmsg="用户不存在")

    news_id = request.json.get("news_id")
    comment_content = request.json.get("comment")
    parent_id = request.json.get("parent_id")  # 子评论

    if not all([news_id, comment_content]):
        return jsonify(errno=RET.PARAMERR, errmsg="参数错误")

    try:
        news_id = int(news_id)
        if parent_id:
            parent_id = int(parent_id)
    except Exception as e:
        current_app.logger.error(e)
        return jsonify(errno=RET.PARAMERR, errmsg="参数错误")

    # 查询新闻是否存在
    try:
        news = News.query.get(news_id)
    except Exception as e:
        current_app.logger.error(e)
        return jsonify(errno=RET.DBERR, errmsg="数据库查询错误")

    if not news:
        return jsonify(errno=RET.NODATA, errmsg="新闻不存在")

    # 定义评论模型
    comment = Comment()
    # 赋值
    comment.user_id = user.id
    comment.news_id = news_id
    comment.content = comment_content
    if parent_id:
        comment.parent_id = parent_id

    # 提交，因为自动提交实在return时提交，但需要再提交时返回数据所以要手动提交
    try:
        db.session.add(comment)
        db.session.commit()
    except Exception as e:
        current_app.logger.error(e)
        db.session.rollback()

    return jsonify(errno=RET.OK, errmsg="OK", data=comment.to_dict())


@news_blue.route("/news_collect", methods=["POST"])
@user_login_data
def collect():
    """
    1. 接收参数
    2. 校验参数
    3. 查询新闻并校验新闻是否存在，并收藏
    :return:
    """
    user = g.user

    if not user:
        return jsonify(errno=RET.SESSIONERR, errmsg="用户未登录")

    news_id = request.json.get("news_id")
    action = request.json.get("action")

    if not all([news_id, action]):
        return jsonify(errno=RET.PARAMERR, errmsg="参数错误")

    try:
        news_id = int(news_id)
    except Exception as e:
        current_app.logger.error(e)
        return jsonify(errno=RET.PARAMERR, errmsg="参数错误")

    if action not in (["collect", "cancel_collect"]):
        return jsonify(errno=RET.PARAMERR, errmsg="参数错误")

    # 查询新闻是否存在
    try:
        news = News.query.get(news_id)
    except Exception as e:
        current_app.logger.error(e)
        return jsonify(errno=RET.DBERR, errmsg="数据库查询错误")

    if not news:
        return jsonify(errno=RET.NODATA, errmsg="新闻不存在")

    # 收藏和取消收藏
    if action == "collect":
        # 收藏新闻
        if news not in user.collection_news:
            user.collection_news.append(news)
    else:
        # 取消收藏
        if news in user.collection_news:
            user.collection_news.remove(news)

    # 返回收藏成功/取消收藏成功
    return jsonify(errno=RET.OK, errmsg="OK")


@news_blue.route('/<int:news_id>')
@user_login_data
def news_detail(news_id):
    """
    新闻详情
    :param news_id:
    :return:
    """

    user = g.user

    # 右上角的新闻排行   news_list是对象不是列表
    news_list = []
    try:
        news_list = News.query.order_by(News.clicks.desc()).limit(6)
    except Exception as e:
        current_app.logger.debug(e)

    # 数据字典列表接受查询到的数据对象
    news_dict_li = []
    for news in news_list:
        news_dict_li.append(news.to_basic_dict())

    news = None

    try:
        news = News.query.get(news_id)
    except Exception as e:
        current_app.logger.error(e)

    if not news:
        # 报404错误，404错误统一显示页面后续再处理
        abort(404)

    # 点击量+1
    news.clicks += 1

    # 当前登录用户是否关注当前新闻作者
    is_followed = False

    is_collected = False
    if user:
        # 是否收藏

        # 多对多直接用中间表去查询，因为是懒加载，所以查询时不用写.all()
        if news in user.collection_news:
            is_collected = True
        if news.user.followers.filter(User.id == g.user.id).count() > 0:
            is_followed = True

    # 查询评论并显示
    comments = []
    try:
        comments = Comment.query.filter(Comment.news_id == news_id).order_by(Comment.create_time.desc()).all()
    except Exception as e:
        current_app.logger.error(e)

    comment_like_ids = []
    if user:
        try:
            # 需求：返回详情页中评论的点赞信息
            # 查询所有评论id，再根据用户id和点赞的评论id查出当前用户都点赞了那些评论，得到本页当前用户点赞的评论id
            comments_ids = [comment.id for comment in comments]
            comment_likes = CommentLike.query.filter(CommentLike.comment_id.in_(comments_ids),
                                                     CommentLike.user_id == user.id).all()

            comment_like_ids = [comment_like.comment_id for comment_like in comment_likes]
        except Exception as e:
            current_app.logger.error(e)

    comment_dict_li = []
    for comment in comments:
        comment_list = comment.to_dict()
        comment_list["is_like"] = False
        if comment.id in comment_like_ids:   # 如果评论id在点赞评论id中，将点赞图片置为True
            comment_list["is_like"] = True
        comment_dict_li.append(comment_list)

    data = {
        "user": user.to_dict() if user else None,
        "news_dict_li": news_dict_li,
        "news": news.to_dict(),
        "is_collected": is_collected,
        "comments": comment_dict_li,
        "is_followed": is_followed
    }
    return render_template("news/detail.html", data=data)
