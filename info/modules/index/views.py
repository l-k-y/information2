from flask import render_template, current_app, session, jsonify, request, g

from . import index_blu
from ... import redis_store
from ...models import User, News, Category
from ...utils.common import user_login_data
from ...utils.response_code import RET


@index_blu.route("/news_list")
def news_list():
    """
    接收新闻分类，页数，每页行数
    :return:
    """
    cid = request.args.get("cid", "1")
    page = request.args.get("page", "1")
    per_page = request.args.get("per_page", "10")

    try:
        cid = int(cid)
        page = int(page)
        per_page = int(per_page)
    except Exception as e:
        current_app.logger.error(e)
        return jsonify(errno=RET.PARAMERR, errmsg="参数错误")
    # 查询条件 cid 默认是1-最新
    filters = [News.status == 0]
    if cid != 1:
        filters.append(News.category_id == cid)
    # 查询数据
    try:
        paginate = News.query.filter(*filters).order_by(News.create_time.desc()).paginate(page, per_page, False)
    except Exception as e:
        current_app.logger.error(e)
        return jsonify(errno=RET.DBERR, errmsg="数据库查询错误")

    news_more_list = paginate.items  # 模型对象列表
    current_page = paginate.page  # 当前页
    total_page = paginate.pages  # 总页数information27

    news_dict_li = []
    for news in news_more_list:
        news_dict_li.append(news.to_basic_dict())
    data = {
        "news_dict_li": news_dict_li,
        "current_page": current_page,
        "total_page": total_page
    }

    return jsonify(errno=RET.OK, errmsg="OK", data=data)


@index_blu.route("/")
@user_login_data
def index():
    """
    取session中的user_id
    :return:
    """
    user = g.user

    # 右上角的新闻排行   news_list是对象不是列表
    news_list = []
    try:
        news_list = News.query.order_by(News.clicks.desc()).limit(6)
    except Exception as e:
        current_app.logger.debug(e)

    # 数据字典列表接受查询到的数据对象
    news_dict_li = []
    for news in news_list:
        news_dict_li.append(news.to_basic_dict())

    # 查询分类信息并返回
    try:
        category_list = Category.query.all()
    except Exception as e:
        current_app.logger.error(e)
        # return jsonify(errno=RET.DBERR, errmsg="数据库查询错误")
    category_li = []
    for category in category_list:
        category_li.append(category.to_dict())

    data = {
        "user": user.to_dict() if user else None,
        "news_dict_li": news_dict_li,
        "category_li": category_li
    }
    return render_template("news/index.html", data=data)


# 访问标题旁边的小图标，在访问网页时自动加载
# send_static_file 查找flask中指定静态文件的方法
@index_blu.route("/favicon.ico")
def favicon():
    # return current_app.send_static_file("news/favicon.ico")
    return current_app.send_static_file("news/favicon.ico")
